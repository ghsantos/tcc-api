const { Sequelize, DataTypes } = require('sequelize');

const sequelize = new Sequelize({
  dialect: 'sqlite',
  storage: './database.sqlite'
});

const StatusData = sequelize.define('Status', {
  uuid: {
    type: DataTypes.UUID,
    defaultValue: Sequelize.UUIDV4,
    primaryKey: true,
  },
  luminance: {
    type: DataTypes.FLOAT,
    allowNull: false,
  },
  topTemperature: {
    type: DataTypes.FLOAT,
    allowNull: false,
  },
  topHumidity: {
    type: DataTypes.FLOAT,
    allowNull: false,
  },
  bottomTemperature: {
    type: DataTypes.FLOAT,
    allowNull: false,
  },
  bottomHumidity: {
    type: DataTypes.FLOAT,
    allowNull: false,
  },
  externalTemperature: {
    type: DataTypes.FLOAT,
    allowNull: false,
  },
  externalHumidity: {
    type: DataTypes.FLOAT,
    allowNull: false,
  },
  waterTemperature: {
    type: DataTypes.FLOAT,
    allowNull: false,
  },
  tank: {
    type: DataTypes.FLOAT,
    allowNull: false,
  },
  led: {
    type: DataTypes.BOOLEAN,
    allowNull: false,
  },
  bump: {
    type: DataTypes.BOOLEAN,
    allowNull: false,
  },
  utc: {
    type: DataTypes.BIGINT,
    allowNull: false,
  },
});

StatusData.sync();

module.exports = {
  sequelize,
  StatusData,
};
