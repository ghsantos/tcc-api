class Status {
  constructor() {
    this.bumpDisable = false;
    this.ledDisable = false;
  }
}

module.exports = Status;
