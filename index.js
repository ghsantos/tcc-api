const express = require('express');
const { Op } = require("sequelize");

const Status = require('./Status')
const { sequelize, StatusData } = require('./Models/StatusData');

const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);

const port = 3000;

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.get('/', (req, res) => {
  res.send('Hello World!');
});

app.get('/data', async (req, res) => {
  const day = Date.now() - 24 * 60 * 60 * 1000

  const query = { where: { utc: { [Op.gte]: day } } };

  const maxTopHumidity = await StatusData.max('topHumidity', query);
  const maxBottomHumidity = await StatusData.max('bottomHumidity', query);
  const maxInternalHumidity = Math.max(maxTopHumidity, maxBottomHumidity);

  const minTopHumidity = await StatusData.min('topHumidity', query);
  const minBottomHumidity = await StatusData.min('bottomHumidity', query);
  const minInternalHumidity = Math.min(minTopHumidity, minBottomHumidity);

  const maxExternalHumidity = await StatusData.max('externalHumidity', query);
  const minExternalHumidity = await StatusData.min('externalHumidity', query);

  const lastData = await StatusData.findOne({
    order: [[ 'createdAt', 'DESC' ]],
  });

  const parsedData = lastData.toJSON();

  res.json({
    ...parsedData,
    maxInternalHumidity,
    minInternalHumidity,
    maxExternalHumidity,
    minExternalHumidity,
  });
});

app.post('/data', async (req, res) => {
  const data = req.body;
  console.log(data);

  const status = await StatusData.create({ ...data, utc: Date.now() });
  res.status(201).json(data);
});

const getHistory = (data, points) => {
  let arr = [];
  let aux = [];

  const sumPoints = Math.ceil(data.length / points);

  data.forEach((item, i) => {
    aux.push(item);
    if ((i + 1) % sumPoints === 0) {
      const result = aux.reduce((acc, cur) => acc + cur, 0) / aux.length;
      arr = [...arr, result];
      aux = [];
    }
  });

  return arr;
}

const getUTCHistory = (data, points) => {
  let arr = [];
  let aux = [];

  const sumPoints = Math.ceil(data.length / points)

  data.forEach((item, i) => {
    aux.push(item);
    if ((i + 1) % sumPoints === 0) {
      const result = aux[Math.round(aux.length / 2) -1];
      arr = [...arr, result];
      aux = [];
    }
  });

  return arr;
}

app.get('/history', async (req, res) => {
  const day = Date.now() - 24 * 60 * 60 * 1000;

  const query = { where: { utc: { [Op.gte]: day } } };

  const data = await StatusData.findAll(query);

  const bottomTemperature = data.map(i => i.bottomTemperature);
  const bottomHumidity = data.map(i => i.bottomHumidity);
  const externalTemperature = data.map(i => i.externalTemperature);
  const externalHumidity = data.map(i => i.externalHumidity);
  const waterTemperature = data.map(i => i.waterTemperature);
  const utc = data.map(i => i.utc);

  const bottomTemperatureHistory = getHistory(bottomTemperature, 48);
  const bottomHumidityHistory = getHistory(bottomHumidity, 48);
  const externalTemperatureHistory = getHistory(externalTemperature, 48);
  const externalHumidityHistory = getHistory(externalHumidity, 48);
  const waterTemperatureHistory = getHistory(waterTemperature, 48);
  const utcHistory = getUTCHistory(utc, 48);

  res.json({
    bottomTemperatureHistory,
    bottomHumidityHistory,
    externalTemperatureHistory,
    externalHumidityHistory,
    waterTemperatureHistory,
    utc: utcHistory,
  })
});

const status = new Status();

io.on('connection', socket => {
  console.log('on connection');
  socket.emit('bumpDisable', status.bumpDisable);
  socket.emit('ledDisable', status.ledDisable);

  socket.on('bumpDisable', data => {
    status.bumpDisable = data;

    socket.broadcast.emit('bumpDisable', data);
  });

  socket.on('ledDisable', data => {
    status.ledDisable = data;

    socket.broadcast.emit('ledDisable', data);
  });
})

const init = async () => {
  try {
    await sequelize.authenticate();
    console.log('Connection has been established successfully.');

    server.listen(port, '0.0.0.0', () => {
      console.log(`Example app listening at 0.0.0.0:${port}`);
    });
  } catch (error) {
    console.error('error:', error);
  }
}

init()
